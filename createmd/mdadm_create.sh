#!/bin/bash

DEVICE1=$1
DEVICE2=$2

if [ -z "$DEVICE2" ]; then
  DEVICE2=/dev/nvme1n1
fi
if [ -z "$DEVICE1" ]; then
  DEVICE1=/dev/nvme0n1
fi

# check if devices exist
if [ ! -e "$DEVICE1" ]; then
  echo "device 1: $DEVICE1 doesn't exist, exiting.."
  exit 1
fi

dd if=/dev/zero of=$DEVICE1 bs=512 count=1 conv=notrunc
if [ ! -e "$DEVICE2" ]; then
  echo "device 2: $DEVICE2 doesn't exist, creating single disk raid.."
  # create a single disk raid, useless but provides consistency for downstream actions
  mdadm --create --force --auto=mdp --metadata=1.0 --verbose /dev/md_d0 --level=mirror --raid-devices=1 $DEVICE1
else
  dd if=/dev/zero of=$DEVICE2 bs=512 count=1 conv=notrunc
  mdadm --create --auto=mdp --metadata=1.0 --verbose /dev/md_d0 --level=mirror --raid-devices=2 $DEVICE1 $DEVICE2
fi

