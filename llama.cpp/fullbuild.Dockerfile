ARG UBUNTU_VERSION=22.04
# This needs to generally match the container host's environment.
# We use a slightly older version for greater compatibility
ARG CUDA_VERSION=12.3.2
# CUDA build image
ARG CUDA_DEV_CONTAINER=nvidia/cuda:${CUDA_VERSION}-devel-ubuntu${UBUNTU_VERSION}
# CUDA runtime image
ARG CUDA_RUN_CONTAINER=nvidia/cuda:${CUDA_VERSION}-runtime-ubuntu${UBUNTU_VERSION}
# CUDA base image (excludes cublas)
ARG CUDA_BASE_CONTAINER=nvidia/cuda:${CUDA_VERSION}-base-ubuntu${UBUNTU_VERSION}

FROM ${CUDA_DEV_CONTAINER} as build

# Unless otherwise specified, we make a fat build.
ARG CUDA_DOCKER_ARCH=all

RUN apt-get update && \
    apt-get install -y build-essential git libcurl4-openssl-dev ccache

WORKDIR /app

RUN git clone https://github.com/ggerganov/llama.cpp.git .

# Set nvcc architecture
ENV CUDA_DOCKER_ARCH=${CUDA_DOCKER_ARCH}
# Enable CUDA
ENV LLAMA_CUDA=1
# Enable cURL
ENV LLAMA_CURL=1

RUN make -j 4

FROM ${CUDA_BASE_CONTAINER} as runtime

# copy server and batched bench
RUN mkdir -p /llama.cpp
COPY --from=build /app/server /app/batched-bench /llama.cpp/

RUN /bin/echo -e '#!/bin/bash\nDEBIAN_FRONTEND=noninteractive\napt-get update && apt-get install -y $@ && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/apt/lists/*' \
    > /usr/local/sbin/apt_install_clean.sh && \
    chmod a+x /usr/local/sbin/apt_install_clean.sh
RUN /bin/echo -e '#!/bin/bash\nDEBIAN_FRONTEND=noninteractive\napt-get update && apt-get remove -y $@ && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/apt/lists/*' \
    > /usr/local/sbin/apt_remove_clean.sh && \
    chmod a+x /usr/local/sbin/apt_remove_clean.sh

# we need just CUDA and CUBLAS
# this saves ~1GB vs the -runtime image
RUN /usr/local/sbin/apt_install_clean.sh libcublas-12-3 libcurl4 openssh-server wget screen curl nano

# ENV variables

# you should override this, defaults to a small file
ENV LLAMA_MU="https://huggingface.co/TheBloke/TinyLlama-1.1B-1T-OpenOrca-GGUF/resolve/main/tinyllama-1.1b-1t-openorca.Q4_0.gguf"

# this parameter is useful for performance tuning
# recommend 2048 for 40GB+ VRAM, 1024 for 24GB VRAM, 512 for less
ENV LLAMA_UB="1024"

# set this to "" to disable flash attention
ENV LLAMA_FA="-fa"
ENV LLAMA_CONTEXT="4096"

ENV LLAMA_ROPE_FREQ_SCALE="1"
ENV LLAMA_PORT="8080"
ENV LLAMA_NP="1"

# set API KEY if desired
ENV LLAMA_API_KEY=""
ENV LLAMA_ADDITIONAL_ARGS="--metrics --host 0.0.0.0 -ngl 200 -cb"

# default mode - just run server
# MODE=BENCH_FIRST - run a short batched-bench to help validate hardware
# MODE=SLEEP - just sleep infinity, allowing shell login to run manual commands
ENV MODE=""

# mount volume here
ENV WORKSPACE="/workspace"

COPY entry.sh .
COPY runpod_rc.sh .
COPY curl_loop.sh .
ENTRYPOINT [ "./entry.sh" ]
